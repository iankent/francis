require 'thor'

module Francis

  class CLI < Thor
    desc "create NAME", "Create a new sinatra app called NAME"
    option :git, :type => :boolean, :aliases => "-g", :desc => "Create app as a git respository and include a .gitignore file"
    option :readme, :type => :boolean, :aliases => "-R", :desc => "Include a readme.md file"
    option :unicorn, :type => :boolean, :aliases => "-u", :desc => "Include a config/unicorn.rb file with default unicorn config"
    option :require_gem, :type => :array, :aliases => "-r", :desc => "Add required gems that will be included in the Gemfile and required within app.rb"
    option :whenever, :type => :boolean, :aliases => "-w", :desc => "Include a config/schedule.rb file"
    def create(name)
      Francis::FileStructure.new(name, options)
    end
  end
  class FileStructure
    def initialize(name, options)
      @name = name
      sinatra_gem_version = Gem.latest_version_for 'sinatra'
      @structure = {
        "/views" => {},
        "/public" => {},
        "/" => {
          "config.ru" => %Q(require 'bundler'\nBundler.require\nrequire_relative 'app'\nrun #{@name.capitalize}),
          "app.rb" => %Q(require 'sinatra'\nclass #{@name.capitalize} < Sinatra::Base\n\tget '/' do\n\t\t'Hello world!'\n\tend\nend),
          "Gemfile" => %Q(source 'https://rubygems.org'\ngem 'sinatra', '#{sinatra_gem_version.approximate_recommendation}'\n)
        }
      }
      @options = options
      @wd = "#{Dir.getwd}/#{@name}"
      parse_options
      execute!
    end
    def add_folder(name)
      @structure["/#{name}"] = {}
    end
    def remove_folder(name)
      @structure.delete("/#{name}"){|el| "#{el} not found"}
    end
    def parse_options
      git
      readme
      unicorn
      whenever
      require_gem
    end
    def execute!
      # create target directory
      Dir.mkdir(@wd)
      # cycle through structure and create folders and files
      @structure.each{|folder,files|
        if folder != "/"
          Dir.mkdir("#{@wd}#{folder}")
        end
        files.each{|name,contents|
          File.open("#{@wd}#{folder}/#{name}", "w"){|f|
            f.write(contents)
          }
        }
      }
      # if git option is given, execute git init command
      if @options[:git]
        Dir.chdir(@wd)
        system "git init"
      end
      puts "#{@name} created as Sinatra app"
    end
    def git
      if @options[:git]
        @structure["/"][".gitignore"] = %Q(.DS_Store\nGemfile.lock\npids/*\nlogs/*\n*.gem)
      end
    end
    def readme
      if @options[:readme]
        @structure["/"]["readme.md"] = %Q(# #{@name.capitalize}\n\nTODO: Write a readme file)
      end
    end
    def unicorn
      if @options[:unicorn]
        add_folder "/config"
        add_folder "/pids"
        add_folder "/logs"
        @structure["/config"]["unicorn.rb"] = %Q(working_directory '#{@wd}'\npid '#{@wd}/pids/unicorn.pid'\nstderr_path '#{@wd}/logs/unicorn.log'\nstdout_path '#{@wd}/logs/unicorn.log'\nlisten '/tmp/#{@name}.sock'\nworker_processes 2\ntimeout 30)
        @structure["/pids"]["unicorn.pid"] = ""
        @structure["/logs"]["unicorn.log"] = ""
      end
    end
    def whenever
      if @options[:whenever]
        add_folder "/config"
        @structure["/config"]["schedule.rb"] = ""
      end
    end
    def require_gem
      if @options[:require_gem]
        @options[:require_gem].each{|r|
          version = Gem.latest_version_for(r)
          if version
            @structure["/"]["Gemfile"] << %Q(gem '#{r}', '#{version.approximate_recommendation}'\n)
          else
            warn "Could not find gem: #{r}, skipping inclusion in Gemfile"
          end
        }
      end
    end
    def add_folder(folder)
      if !@structure.has_key?(folder)
        @structure["#{folder}"] = {}
      end
    end
  end
end
