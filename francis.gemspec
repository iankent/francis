require 'date'
Gem::Specification.new do |s|
  s.name        = 'francis'
  s.version     = '2.0.0'
  s.date        = Date.today.iso8601
  s.summary     = "Francis"
  s.description = "A command line tool for quickly setting up Sinatra apps"
  s.license     = "MIT"
  s.authors     = ["Ian Kent"]
  s.email       = 'ian@iankent.me'
  s.files       = ["lib/francis.rb"]
  s.executables << 'francis'
  s.homepage    = 'https://bitbucket.org/iankentforrestbrown/francis/'
  s.add_dependency 'thor', '~> 1.1'
end
