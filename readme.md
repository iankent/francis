# Francis

A command line tool for quickly setting up Sinatra apps. More info at [iankent.me](http://iankent.me).

## Usage

```
francis create NAME
```

### Options

```
-g, [--git], [--no-git]            # Create app as a git respository and include a .gitignore file
-R, [--readme], [--no-readme]      # Include a readme.md file
-u, [--unicorn], [--no-unicorn]    # Include a config/unicorn.rb file with default unicorn config
-r, [--require-gem=one two three]  # Add required gems that will be included in the Gemfile and required within app.rb
-w, [--whenever], [--no-whenever]  # Include a config/schedule.rb file
```

## Installation

```
gem install francis
```
